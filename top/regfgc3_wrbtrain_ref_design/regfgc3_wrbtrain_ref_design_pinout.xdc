####################################################################
#############################PINOUT#################################
####################################################################
####################################################################

#System Clock
set_property PACKAGE_PIN 	G16 [get_ports FPGA_SYS_CLK_i]

#SCI Interface
set_property PACKAGE_PIN	A13	[get_ports 	FPGA_SCIVS_TX_EN_o	]
set_property PACKAGE_PIN	A14	[get_ports 	FPGA_SCIVS_RX_i	]
set_property PACKAGE_PIN	B14	[get_ports 	FPGA_SCIVS_TX_o	]
set_property PACKAGE_PIN	C15	[get_ports 	FPGA_SCIVS_MODE_i	]

#QSPI Interface
set_property PACKAGE_PIN	B12	[get_ports 	FPGA_QSPI_DIN_i	]
set_property PACKAGE_PIN	B13	[get_ports 	FPGA_QSPI_DOUT_o	]
set_property PACKAGE_PIN	T16	[get_ports 	FPGA_QSPI_CLK_i	]

#LEDs
set_property PACKAGE_PIN	B18	[get_ports 	FPGA_LD1_GREEN_o	]
set_property PACKAGE_PIN	D15	[get_ports 	FPGA_LD2_RED_o	]
set_property PACKAGE_PIN	D17	[get_ports 	FPGA_LD2_GREEN_o	]
set_property PACKAGE_PIN	E17	[get_ports 	FPGA_LD1_BLUE_o	]
set_property PACKAGE_PIN	A15	[get_ports 	FPGA_LD2_BLUE_o	]

#SPI Interface
set_property PACKAGE_PIN	B17	[get_ports 	FPGA_SPIVS_CLK_i	]
set_property PACKAGE_PIN	C14	[get_ports 	FPGA_SPIVS_TX_DIR_o	]
set_property PACKAGE_PIN	A16	[get_ports 	FPGA_SPIVS_TX_o	]
set_property PACKAGE_PIN	B16	[get_ports 	FPGA_SPIVS_RX_i	]

#FMC specific signals
#set_property PACKAGE_PIN	M2	[get_ports 	FPGA_FMC_SCL_o	]
#set_property PACKAGE_PIN	N1	[get_ports 	FPGA_FMC_SDA	]
set_property PACKAGE_PIN	N2	[get_ports 	FPGA_FMC_TMS_o	]
set_property PACKAGE_PIN	K1	[get_ports 	FPGA_FMC_TDO_i	]
set_property PACKAGE_PIN	H5	[get_ports 	FPGA_FMC_TDI_o	]
set_property PACKAGE_PIN	K2	[get_ports 	FPGA_FMC_TCK_o	]
set_property PACKAGE_PIN    M3	[get_ports 	FPGA_FMC_TRST_L_o	]

#FMC pins
set_property PACKAGE_PIN	A4	[get_ports 	{FPGA_FMC_LA_P[0]}	] ;# LA00_P - ok
set_property PACKAGE_PIN	A3	[get_ports 	{FPGA_FMC_LA_N[0]}	] ;# LA00_N - ok
set_property PACKAGE_PIN	B3	[get_ports 	{FPGA_FMC_LA_P[1]}	] ;# LA01_P - ok
set_property PACKAGE_PIN	B2	[get_ports 	{FPGA_FMC_LA_N[1]}	] ;# LA01_N - ok
set_property PACKAGE_PIN	B1	[get_ports 	{rst_from_carrier_n_o}] ;# LA02_P - ok
set_property PACKAGE_PIN	A1	[get_ports 	{rst_to_carrier_n_i }] ;# LA02_N - ok
set_property PACKAGE_PIN	E2	[get_ports 	{FPGA_FMC_LA_P[2]}	] ;# LA03_P - ok
set_property PACKAGE_PIN	D2	[get_ports 	{FPGA_FMC_LA_N[2]}	] ;# LA03_N - ok
set_property PACKAGE_PIN	C4	[get_ports 	{FPGA_FMC_LA_P[3]}	] ;# LA04_P - ok
set_property PACKAGE_PIN	B4	[get_ports 	{FPGA_FMC_LA_N[3]}	] ;# LA04_N - ok
set_property PACKAGE_PIN	D5	[get_ports 	{FPGA_FMC_LA_P[4]}	] ;# LA05_P - ok
set_property PACKAGE_PIN	D4	[get_ports 	{FPGA_FMC_LA_N[4]}	] ;# LA05_N - ok
set_property PACKAGE_PIN	F1	[get_ports 	{FPGA_FMC_LA_P[5]}	] ;# LA06_P - ok
set_property PACKAGE_PIN	E1	[get_ports 	{FPGA_FMC_LA_N[5]}	] ;# LA06_N - ok
set_property PACKAGE_PIN	E3	[get_ports 	{link_up_i}	        ] ;# LA07_P - ok
set_property PACKAGE_PIN	D3	[get_ports 	{time_valid_i}	    ] ;# LA07_N - ok
set_property PACKAGE_PIN	K5	[get_ports 	{FPGA_FMC_LA_P[6]}	] ;# LA08_P - ok
set_property PACKAGE_PIN	L4	[get_ports 	{FPGA_FMC_LA_N[6]}	] ;# LA08_N - ok
set_property PACKAGE_PIN	E6	[get_ports 	{rx_irq_i}	        ] ;# LA09_P - ok
set_property PACKAGE_PIN	E5	[get_ports 	{irq_aux_i}	        ] ;# LA09_N - ok
set_property PACKAGE_PIN	J3	[get_ports 	{rx_wb_adr_o[0]}	] ;# LA10_P - ok
set_property PACKAGE_PIN	J2	[get_ports 	{rx_wb_adr_o[1]}	] ;# LA10_N - ok
set_property PACKAGE_PIN	E7	[get_ports 	{rx_wb_adr_o[2]}	] ;# LA11_P - ok
set_property PACKAGE_PIN	D7	[get_ports 	{rx_wb_adr_o[3]}	] ;# LA11_N - ok
set_property PACKAGE_PIN	K3	[get_ports 	{rx_wb_adr_o[4]}	] ;# LA12_P - ok
set_property PACKAGE_PIN	L3	[get_ports 	{rx_wb_adr_o[5]}	] ;# LA12_N - ok
set_property PACKAGE_PIN	J4	[get_ports 	{rx_wb_adr_o[6]}	] ;# LA13_P - ok
set_property PACKAGE_PIN	H4	[get_ports 	{rx_wb_adr_o[7]}	] ;# LA13_N - ok
set_property PACKAGE_PIN	N5	[get_ports 	{rx_wb_dat_b[0]}	] ;# LA14_P - ok
set_property PACKAGE_PIN	P5	[get_ports 	{rx_wb_dat_b[1]}	] ;# LA14_N - ok
set_property PACKAGE_PIN	L6	[get_ports 	{rx_wb_dat_b[2]}	] ;# LA15_P - ok
set_property PACKAGE_PIN	L5	[get_ports 	{rx_wb_dat_b[3]}	] ;# LA15_N - ok
set_property PACKAGE_PIN	M6	[get_ports 	{rx_wb_dat_b[4]}    ] ;# LA16_P - ok
set_property PACKAGE_PIN	N6	[get_ports 	{rx_wb_dat_b[5]}    ] ;# LA16_N - ok
set_property PACKAGE_PIN	P2	[get_ports 	{rx_wb_dat_b[6]}	] ;# LA17_P - ok
set_property PACKAGE_PIN	R2	[get_ports 	{rx_wb_dat_b[7]}	] ;# LA17_N - ok
set_property PACKAGE_PIN	U1	[get_ports 	{rx_wb_ack_i}	    ] ;# LA18_P - ok
set_property PACKAGE_PIN	V1	[get_ports 	{rx_wb_we_o}	    ] ;# LA18_N - ok
set_property PACKAGE_PIN	L1	[get_ports 	{rx_wb_cyc_o}	    ] ;# LA19_P - ok
set_property PACKAGE_PIN	M1	[get_ports 	{rx_wb_stb_o}	    ] ;# LA19_N - ok
set_property PACKAGE_PIN	R1	[get_ports 	{rx_wb_clk_o}	    ] ;# LA20_P - ok
set_property PACKAGE_PIN	T1	[get_ports 	{tx_wb_clk_o}	    ] ;# LA20_N - ok
set_property PACKAGE_PIN	P4	[get_ports 	{rx_wb_rty_i}	    ] ;# LA21_P - ok
set_property PACKAGE_PIN	P3	[get_ports 	{rx_wb_stall_i}	    ] ;# LA21_N - ok
set_property PACKAGE_PIN	U2	[get_ports 	{tx_wb_adr_o[0]}	] ;# LA22_P - ok
set_property PACKAGE_PIN	V2	[get_ports 	{tx_wb_adr_o[1]}	] ;# LA22_N - ok
set_property PACKAGE_PIN	M4	[get_ports 	{tx_wb_adr_o[2]}	] ;# LA23_P - ok
set_property PACKAGE_PIN	N4	[get_ports 	{tx_wb_adr_o[3]}	] ;# LA23_N - ok
set_property PACKAGE_PIN	T5	[get_ports 	{tx_wb_adr_o[4]}	] ;# LA24_P - ok
set_property PACKAGE_PIN	T4	[get_ports 	{tx_wb_adr_o[5]}	] ;# LA24_N - ok
set_property PACKAGE_PIN	V5	[get_ports 	{tx_wb_adr_o[6]}	] ;# LA25_P - ok
set_property PACKAGE_PIN	V4	[get_ports 	{tx_wb_adr_o[7]}	] ;# LA25_N - ok
set_property PACKAGE_PIN	R3	[get_ports 	{tx_wb_dat_b[0]}	] ;# LA26_P - ok
set_property PACKAGE_PIN	T3	[get_ports 	{tx_wb_dat_b[1]}	] ;# LA26_N - ok
set_property PACKAGE_PIN	U4	[get_ports 	{tx_wb_dat_b[2]}	] ;# LA27_P - ok (corrected)
set_property PACKAGE_PIN	U3	[get_ports 	{tx_wb_dat_b[3]}	] ;# LA27_N - ok (corrected)	
set_property PACKAGE_PIN	U7	[get_ports 	{tx_wb_dat_b[4]}	] ;# LA28_P - ok
set_property PACKAGE_PIN	U6	[get_ports 	{tx_wb_dat_b[5]}	] ;# LA28_N - ok
set_property PACKAGE_PIN	R7	[get_ports 	{tx_wb_dat_b[6]}	] ;# LA29_P - ok
set_property PACKAGE_PIN	T6	[get_ports 	{tx_wb_dat_b[7]}	] ;# LA29_N - ok
set_property PACKAGE_PIN	R8	[get_ports 	{rx_wb_err_i}	    ] ;# LA30_P - ok
set_property PACKAGE_PIN	T8	[get_ports 	{tx_wb_we_o}	    ] ;# LA30_N - ok
set_property PACKAGE_PIN	V7	[get_ports 	{tx_wb_cyc_o}	    ] ;# LA31_P - ok (corrected)
set_property PACKAGE_PIN	V6	[get_ports 	{tx_wb_stb_o}	    ] ;# LA31_N - ok (corrected)
set_property PACKAGE_PIN	U9	[get_ports 	{tx_wb_ack_i}	    ] ;# LA32_P - ok
set_property PACKAGE_PIN	V9	[get_ports 	{tx_wb_err_i}	    ] ;# LA32_N - ok
set_property PACKAGE_PIN	R6	[get_ports 	{tx_wb_rty_i}	    ] ;# LA33_P - ok
set_property PACKAGE_PIN	R5	[get_ports 	{tx_wb_stall_i}	    ] ;# LA33_N - ok

#set_property PACKAGE_PIN	B6	[get_ports 	FPGA_FMC_DP0C2M_N	]
#set_property PACKAGE_PIN	B7	[get_ports 	FPGA_FMC_DP0C2M_P	]
#set_property PACKAGE_PIN	A5	[get_ports 	FPGA_FMC_GBTCLK0M2C_N	]
#set_property PACKAGE_PIN	A6	[get_ports 	FPGA_FMC_GBTCLK0M2C_P	]
#set_property PACKAGE_PIN	C5	[get_ports 	FPGA_FMC_DP0M2C_N	]
#set_property PACKAGE_PIN	C6	[get_ports 	FPGA_FMC_DP0M2C_P	]
set_property PACKAGE_PIN	C7	[get_ports 	clk_sys_62m5_n_i	]
set_property PACKAGE_PIN	D8	[get_ports 	clk_sys_62m5_p_i	]
#set_property PACKAGE_PIN	F3	[get_ports 	FPGA_FMC_CLK1M2C_N	]
#set_property PACKAGE_PIN	F4	[get_ports 	FPGA_FMC_CLK1M2C_P	]
#set_property PACKAGE_PIN	F6	[get_ports 	FPGA_FMC_VREF_A_M2C	]
set_property PACKAGE_PIN	H6	[get_ports 	FPGA_FMC_PRSNT_M2C_L_i	]


#BUSTYPE signals
set_property PACKAGE_PIN	A18	[get_ports 	{BUS_TYPE_i[0]}	]
set_property PACKAGE_PIN	C17	[get_ports 	{BUS_TYPE_i[1]}	]
set_property PACKAGE_PIN	C16	[get_ports 	{BUS_TYPE_i[2]}	]
set_property PACKAGE_PIN	D14	[get_ports 	{BUS_TYPE_i[3]}	]
set_property PACKAGE_PIN	E18	[get_ports 	{BUS_TYPE_i[4]}	]
set_property PACKAGE_PIN	F13	[get_ports 	{BUS_TYPE_i[5]}	]

#Agilent Debug signals
set_property PACKAGE_PIN	R16	[get_ports 	FPGA_AGILENT_CLK	]
set_property PACKAGE_PIN	A11	[get_ports 	{FPGA_AGILENT_D[0]}	]
set_property PACKAGE_PIN	B11	[get_ports 	{FPGA_AGILENT_D[1]}	]
set_property PACKAGE_PIN	C12	[get_ports 	{FPGA_AGILENT_D[2]}	]
set_property PACKAGE_PIN	D13	[get_ports 	{FPGA_AGILENT_D[3]}	]
set_property PACKAGE_PIN	H16	[get_ports 	{FPGA_AGILENT_D[4]}	]
set_property PACKAGE_PIN	D12	[get_ports 	{FPGA_AGILENT_D[5]}	]
set_property PACKAGE_PIN	D18	[get_ports 	{FPGA_AGILENT_D[6]}	]
set_property PACKAGE_PIN	E15	[get_ports 	{FPGA_AGILENT_D[7]}	]
set_property PACKAGE_PIN	E16	[get_ports 	{FPGA_AGILENT_D[8]}	]
set_property PACKAGE_PIN	F14	[get_ports 	{FPGA_AGILENT_D[9]}	]
set_property PACKAGE_PIN	F16	[get_ports 	{FPGA_AGILENT_D[10]}	]
set_property PACKAGE_PIN	J17	[get_ports 	{FPGA_AGILENT_D[11]}	]
set_property PACKAGE_PIN	J14	[get_ports 	{FPGA_AGILENT_D[12]}	]
set_property PACKAGE_PIN	L18	[get_ports 	{FPGA_AGILENT_D[13]}	]
set_property PACKAGE_PIN	N17	[get_ports 	{FPGA_AGILENT_D[14]}	]
set_property PACKAGE_PIN	R18	[get_ports 	{FPGA_AGILENT_D[15]}	]


set_property PACKAGE_PIN	L16	[get_ports 	{FPGA_FGC3_CMD_i[0]}	]
set_property PACKAGE_PIN	P14	[get_ports 	{FPGA_FGC3_CMD_i[1]}	]
set_property PACKAGE_PIN	R17	[get_ports 	{FPGA_FGC3_CMD_i[2]}	]
set_property PACKAGE_PIN	T18	[get_ports 	{FPGA_FGC3_CMD_i[3]}	]
set_property PACKAGE_PIN	K16	[get_ports 	{FPGA_FGC3_CMD_i[4]}	]
set_property PACKAGE_PIN	N14	[get_ports 	{FPGA_FGC3_CMD_i[5]}	]
set_property PACKAGE_PIN	H17	[get_ports 	{FPGA_FGC3_CMD_i[6]}	]
set_property PACKAGE_PIN	M17	[get_ports 	{FPGA_FGC3_CMD_i[7]}	]

set_property PACKAGE_PIN	T13	[get_ports 	{FPGA_EXEC_READ_i[0]}	]
set_property PACKAGE_PIN	M13	[get_ports 	{FPGA_EXEC_READ_i[1]}	]
set_property PACKAGE_PIN	N16	[get_ports 	{FPGA_EXEC_READ_i[2]}	]
set_property PACKAGE_PIN	G14	[get_ports 	{FPGA_EXEC_READ_i[3]}	]

#Option PINs
set_property PACKAGE_PIN	R13	[get_ports 	{FPGA_OPTION_SW_i[1]}	]
set_property PACKAGE_PIN	V14	[get_ports 	{FPGA_OPTION_SW_i[2]}	]
set_property PACKAGE_PIN	V15	[get_ports 	{FPGA_OPTION_SW_i[3]}	]
set_property PACKAGE_PIN	R12	[get_ports 	{FPGA_OPTION_SW_i[4]}	]
set_property PACKAGE_PIN	T14	[get_ports 	{FPGA_OPTION_SW_i[5]}	]
set_property PACKAGE_PIN	R15	[get_ports 	{FPGA_OPTION_SW_i[6]}	]
set_property PACKAGE_PIN	P15	[get_ports 	{FPGA_OPTION_SW_i[7]}	]
set_property PACKAGE_PIN	P18	[get_ports 	{FPGA_OPTION_SW_i[8]}	]


#DIGSUPA
set_property PACKAGE_PIN	G13	[get_ports 	{FPGA_DIG_SUPA_i[0]}	]
set_property PACKAGE_PIN	M18	[get_ports 	{FPGA_DIG_SUPA_i[1]} ]
set_property PACKAGE_PIN	M16	[get_ports 	{FPGA_DIG_SUPA_i[2]}	]
set_property PACKAGE_PIN	N15	[get_ports 	{FPGA_DIG_SUPA_i[3]}	]
set_property PACKAGE_PIN	P17	[get_ports 	{FPGA_DIG_SUPA_i[4]}	]

#DIGSUPB and Direction
set_property PACKAGE_PIN	T11	[get_ports 	{FPGA_DIR_DIG_SUPB_o[0]}	]
set_property PACKAGE_PIN	U12	[get_ports 	{FPGA_DIR_DIG_SUPB_o[1]}	]
set_property PACKAGE_PIN	R10	[get_ports 	{FPGA_DIR_DIG_SUPB_o[2]}	]
set_property PACKAGE_PIN	T9	[get_ports 	{FPGA_DIR_DIG_SUPB_o[3]}	]
set_property PACKAGE_PIN	R11	[get_ports 	{FPGA_DIR_DIG_SUPB_o[4]}	]

set_property PACKAGE_PIN	V12	[get_ports 	{FPGA_DIG_SUPB[0]}	]
set_property PACKAGE_PIN	V11	[get_ports 	{FPGA_DIG_SUPB[1]}	]
set_property PACKAGE_PIN	U11	[get_ports 	{FPGA_DIG_SUPB[2]}	]
set_property PACKAGE_PIN	V10	[get_ports 	{FPGA_DIG_SUPB[3]}	]
set_property PACKAGE_PIN	T10	[get_ports 	{FPGA_DIG_SUPB[4]}	]

#FGC STATUS
set_property PACKAGE_PIN	F18	[get_ports 	{FPGA_FGC3_STATUS_i[0]}	]
set_property PACKAGE_PIN	G17	[get_ports 	{FPGA_FGC3_STATUS_i[1]}	]
set_property PACKAGE_PIN	J18	[get_ports 	{FPGA_FGC3_STATUS_i[2]}	]
set_property PACKAGE_PIN	J15	[get_ports 	{FPGA_FGC3_STATUS_i[3]}	]
set_property PACKAGE_PIN	G18	[get_ports 	{FPGA_FGC3_STATUS_i[4]}	]
set_property PACKAGE_PIN	H14	[get_ports 	{FPGA_FGC3_STATUS_i[5]}	]
set_property PACKAGE_PIN	H15	[get_ports 	{FPGA_FGC3_STATUS_i[6]}	]
set_property PACKAGE_PIN	k15	[get_ports 	{FPGA_FGC3_STATUS_i[7]}	]
set_property PACKAGE_PIN	U17	[get_ports 	{FPGA_FGC3_STATUS_i[8]}	]
set_property PACKAGE_PIN	U16	[get_ports 	{FPGA_FGC3_STATUS_i[9]}	]
set_property PACKAGE_PIN	T15	[get_ports 	{FPGA_FGC3_STATUS_i[10]}	]
set_property PACKAGE_PIN	U13	[get_ports 	{FPGA_FGC3_STATUS_i[11]}	]
set_property PACKAGE_PIN	U18	[get_ports 	{FPGA_FGC3_STATUS_i[12]}	]
set_property PACKAGE_PIN	V17	[get_ports 	{FPGA_FGC3_STATUS_i[13]}	]
set_property PACKAGE_PIN	V16	[get_ports 	{FPGA_FGC3_STATUS_i[14]}	]
set_property PACKAGE_PIN	U14	[get_ports 	{FPGA_FGC3_STATUS_i[15]}	]

#POF TX/RX
set_property PACKAGE_PIN	A8	[get_ports 	FPGA_POF_RX_2_i	]
set_property PACKAGE_PIN	A10	[get_ports 	FPGA_POF_TX_1_o	]
set_property PACKAGE_PIN	A9	[get_ports 	FPGA_POF_TX_2_o	]
set_property PACKAGE_PIN	B8	[get_ports 	FPGA_POF_RX_1_i	]

####################################################################
###########################Configuration############################
####################################################################
####################################################################
set_property CONFIG_VOLTAGE 3.3 [current_design]
set_property CFGBVS VCCO [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 12 [current_design]
set_property BITSTREAM.CONFIG.DONEPIN PULLUP [current_design]
set_property BITSTREAM.STARTUP.STARTUPCLK CCLK [current_design]
set_property BITSTREAM.CONFIG.SPI_FALL_EDGE YES [current_design]
set_property BITSTREAM.CONFIG.CCLKPIN PULLUP [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 1 [current_design]
set_property BITSTREAM.CONFIG.UNUSEDPIN PULLDOWN [current_design]

create_clock -period 25 -name FPGA_SYS_CLK_i -waveform {0.000 12.500} [get_ports FPGA_SYS_CLK_i]
set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets FPGA_SYS_CLK_i_IBUF]
#set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets clk_sys_62m5]
#set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets rx_wb_clk_o_OBUF]
#set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets tx_wb_clk_o_OBUF]
####################################################################
############Definition of electrical standard for pins##############
####################################################################
####################################################################

set_property IOSTANDARD LVCMOS33 [get_ports {BUS_TYPE_i[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {BUS_TYPE_i[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {BUS_TYPE_i[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {BUS_TYPE_i[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {BUS_TYPE_i[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_AGILENT_D[15]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_AGILENT_D[14]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_AGILENT_D[13]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_AGILENT_D[12]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_AGILENT_D[11]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_AGILENT_D[10]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_AGILENT_D[9]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_AGILENT_D[8]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_AGILENT_D[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_AGILENT_D[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_AGILENT_D[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_AGILENT_D[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_AGILENT_D[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_AGILENT_D[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_AGILENT_D[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_AGILENT_D[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {BUS_TYPE_i[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_DIG_SUPA_i[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_DIG_SUPA_i[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_DIG_SUPA_i[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_DIG_SUPA_i[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_DIG_SUPA_i[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_DIG_SUPB[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_DIG_SUPB[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_DIG_SUPB[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_DIG_SUPB[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_DIG_SUPB[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_DIR_DIG_SUPB_o[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_DIR_DIG_SUPB_o[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_DIR_DIG_SUPB_o[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_DIR_DIG_SUPB_o[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_DIR_DIG_SUPB_o[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_EXEC_READ_i[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_EXEC_READ_i[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_EXEC_READ_i[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_EXEC_READ_i[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_CMD_i[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_CMD_i[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_CMD_i[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_CMD_i[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_CMD_i[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_CMD_i[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_CMD_i[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_CMD_i[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_STATUS_i[15]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_STATUS_i[14]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_STATUS_i[13]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_STATUS_i[12]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_STATUS_i[11]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_STATUS_i[10]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_STATUS_i[9]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_STATUS_i[8]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_STATUS_i[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_STATUS_i[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_STATUS_i[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_STATUS_i[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_STATUS_i[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_STATUS_i[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_STATUS_i[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_FGC3_STATUS_i[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_OPTION_SW_i[8]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_OPTION_SW_i[7]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_OPTION_SW_i[6]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_OPTION_SW_i[5]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_OPTION_SW_i[4]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_OPTION_SW_i[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_OPTION_SW_i[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {FPGA_OPTION_SW_i[1]}]
set_property PULLUP true [get_ports {BUS_TYPE_i[4]}]
set_property PULLUP true [get_ports {BUS_TYPE_i[3]}]
set_property PULLUP true [get_ports {BUS_TYPE_i[2]}]
set_property PULLUP true [get_ports {BUS_TYPE_i[0]}]
set_property PULLUP true [get_ports {BUS_TYPE_i[1]}]
set_property PULLUP true [get_ports {FPGA_EXEC_READ_i[3]}]
set_property PULLUP true [get_ports {FPGA_EXEC_READ_i[2]}]
set_property PULLUP true [get_ports {FPGA_EXEC_READ_i[1]}]
set_property PULLUP true [get_ports {FPGA_EXEC_READ_i[0]}]
set_property PULLUP true [get_ports {FPGA_OPTION_SW_i[8]}]
set_property PULLUP true [get_ports {FPGA_OPTION_SW_i[7]}]
set_property PULLUP true [get_ports {FPGA_OPTION_SW_i[6]}]
set_property PULLUP true [get_ports {FPGA_OPTION_SW_i[5]}]
set_property PULLUP true [get_ports {FPGA_OPTION_SW_i[4]}]
set_property PULLUP true [get_ports {FPGA_OPTION_SW_i[3]}]
set_property PULLUP true [get_ports {FPGA_OPTION_SW_i[2]}]
set_property PULLUP true [get_ports {FPGA_OPTION_SW_i[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_AGILENT_CLK]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_LD1_BLUE_o]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_LD1_GREEN_o]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_LD2_BLUE_o]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_LD2_GREEN_o]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_LD2_RED_o]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_POF_RX_1_i]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_POF_RX_2_i]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_POF_TX_1_o]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_POF_TX_2_o]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_QSPI_CLK_i]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_QSPI_DIN_i]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_QSPI_DOUT_o]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_SCIVS_MODE_i]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_SCIVS_RX_i]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_SPIVS_RX_i]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_SCIVS_TX_EN_o]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_SPIVS_TX_DIR_o]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_SCIVS_TX_o]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_SPIVS_TX_o]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_SPIVS_CLK_i]
set_property IOSTANDARD LVCMOS33 [get_ports FPGA_SYS_CLK_i]


#FMC specific signals
#set_property IOSTANDARD LVCMOS25	[get_ports 	FPGA_FMC_SCL_o	]
#set_property IOSTANDARD LVCMOS25	[get_ports 	FPGA_FMC_SDA	]
set_property IOSTANDARD LVCMOS25	[get_ports 	FPGA_FMC_PRSNT_M2C_L_i	]
set_property IOSTANDARD LVCMOS25	[get_ports 	FPGA_FMC_TDO_i	]
set_property IOSTANDARD LVCMOS25	[get_ports 	FPGA_FMC_TCK_o	]
set_property IOSTANDARD LVCMOS25	[get_ports 	FPGA_FMC_TDI_o	]
set_property IOSTANDARD LVCMOS25	[get_ports 	FPGA_FMC_TMS_o	]
set_property IOSTANDARD LVCMOS25	[get_ports 	FPGA_FMC_TRST_L_o	]
set_property PULLUP true            [get_ports FPGA_FMC_PRSNT_M2C_L_i]

#FMC differential pairs
set_property IOSTANDARD LVDS_25 	[get_ports 	{FPGA_FMC_LA_P[0]}	] ;# LA00_P
set_property IOSTANDARD LVDS_25 	[get_ports 	{FPGA_FMC_LA_P[1]}	] ;# LA01_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rst_from_carrier_n_o}	] ;# LA02_P
set_property IOSTANDARD LVDS_25 	[get_ports 	{FPGA_FMC_LA_P[2]}	] ;# LA03_P
set_property IOSTANDARD LVDS_25 	[get_ports 	{FPGA_FMC_LA_P[3]}	] ;# LA04_P
set_property IOSTANDARD LVDS_25 	[get_ports 	{FPGA_FMC_LA_P[4]}	] ;# LA05_P
set_property IOSTANDARD LVDS_25 	[get_ports 	{FPGA_FMC_LA_P[5]}	] ;# LA06_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{link_up_i}	        ] ;# LA07_P
set_property IOSTANDARD LVDS_25 	[get_ports 	{FPGA_FMC_LA_P[6]}	] ;# LA08_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_irq_i}	        ] ;# LA09_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_adr_o[0]}	] ;# LA10_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_adr_o[2]}	] ;# LA11_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_adr_o[4]}	] ;# LA12_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_adr_o[6]}	] ;# LA13_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_dat_b[0]}	] ;# LA14_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_dat_b[2]}	] ;# LA15_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_dat_b[4]}	] ;# LA16_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_dat_b[6]}	] ;# LA17_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_ack_i}	    ] ;# LA18_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_cyc_o}	    ] ;# LA19_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_clk_o}	    ] ;# LA20_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_rty_i}	    ] ;# LA21_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_adr_o[0]}	] ;# LA22_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_adr_o[2]}	] ;# LA23_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_adr_o[4]}	] ;# LA24_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_adr_o[6]}	] ;# LA25_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_dat_b[0]}	] ;# LA26_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_dat_b[2]}	] ;# LA27_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_dat_b[4]}	] ;# LA28_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_dat_b[6]}	] ;# LA29_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_err_i}	    ] ;# LA30_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_cyc_o}	    ] ;# LA31_P  
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_ack_i}	    ] ;# LA32_P
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_rty_i}	    ] ;# LA33_P                                                           

set_property IOSTANDARD LVDS_25 	[get_ports 	{clk_sys_62m5_n_i}]
set_property IOSTANDARD LVDS_25 	[get_ports 	{clk_sys_62m5_p_i}]

set_property IOSTANDARD LVDS_25 	[get_ports 	{FPGA_FMC_LA_N[0]}	] ;# LA00_N 
set_property IOSTANDARD LVDS_25 	[get_ports 	{FPGA_FMC_LA_N[1]}	] ;# LA01_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rst_to_carrier_n_i}	] ;# LA02_N
set_property IOSTANDARD LVDS_25 	[get_ports 	{FPGA_FMC_LA_N[2]}	] ;# LA03_N
set_property IOSTANDARD LVDS_25 	[get_ports 	{FPGA_FMC_LA_N[3]}	] ;# LA04_N
set_property IOSTANDARD LVDS_25 	[get_ports 	{FPGA_FMC_LA_N[4]}	] ;# LA05_N
set_property IOSTANDARD LVDS_25 	[get_ports 	{FPGA_FMC_LA_N[5]}	] ;# LA06_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{time_valid_i}	    ] ;# LA07_N
set_property IOSTANDARD LVDS_25 	[get_ports 	{FPGA_FMC_LA_N[6]}	] ;# LA08_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{irq_aux_i}	        ] ;# LA09_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_adr_o[1]}	] ;# LA10_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_adr_o[3]}	] ;# LA11_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_adr_o[5]}	] ;# LA12_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_adr_o[7]}	] ;# LA13_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_dat_b[1]}	] ;# LA14_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_dat_b[3]}	] ;# LA15_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_dat_b[5]}	] ;# LA16_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_dat_b[7]}	] ;# LA17_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_we_o}	    ] ;# LA18_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_stb_o}	    ] ;# LA19_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_clk_o}	    ] ;# LA20_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{rx_wb_stall_i}	    ] ;# LA21_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_adr_o[1]}	] ;# LA22_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_adr_o[3]}	] ;# LA23_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_adr_o[5]}	] ;# LA24_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_adr_o[7]}	] ;# LA25_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_dat_b[1]}	] ;# LA26_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_dat_b[3]}	] ;# LA27_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_dat_b[5]}	] ;# LA28_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_dat_b[7]}	] ;# LA29_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_we_o}    	] ;# LA30_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_stb_o}	    ] ;# LA31_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_err_i}	    ] ;# LA32_N
set_property IOSTANDARD LVCMOS25 	[get_ports 	{tx_wb_stall_i}    	] ;# LA33_N

####################################################################
##################### Force flip-flops into IOB ####################
####################################################################
####################################################################

#FMC pins
#set_property IOB TRUE	[get_ports 	{rx_wb_adr_o[0]}	] ;# LA10_P
#set_property IOB TRUE	[get_ports 	{rx_wb_adr_o[1]}	] ;# LA10_N
#set_property IOB TRUE	[get_ports 	{rx_wb_adr_o[2]}	] ;# LA11_P
#set_property IOB TRUE	[get_ports 	{rx_wb_adr_o[3]}	] ;# LA11_N
#set_property IOB TRUE	[get_ports 	{rx_wb_adr_o[4]}	] ;# LA12_P
#set_property IOB TRUE	[get_ports 	{rx_wb_adr_o[5]}	] ;# LA12_N
#set_property IOB TRUE	[get_ports 	{rx_wb_adr_o[6]}	] ;# LA13_P
#set_property IOB TRUE	[get_ports 	{rx_wb_adr_o[7]}	] ;# LA13_N
#set_property IOB TRUE	[get_ports 	{rx_wb_dat_b[0]}	] ;# LA14_P
#set_property IOB TRUE	[get_ports 	{rx_wb_dat_b[1]}	] ;# LA14_N
#set_property IOB TRUE	[get_ports 	{rx_wb_dat_b[2]}	] ;# LA15_P
#set_property IOB TRUE	[get_ports 	{rx_wb_dat_b[3]}	] ;# LA15_N
#set_property IOB TRUE	[get_ports 	{rx_wb_dat_b[4]}    ] ;# LA16_P
#set_property IOB TRUE	[get_ports 	{rx_wb_dat_b[5]}    ] ;# LA16_N
#set_property IOB TRUE	[get_ports 	{rx_wb_dat_b[6]}	] ;# LA17_P
#set_property IOB TRUE	[get_ports 	{rx_wb_dat_b[7]}	] ;# LA17_N
#set_property IOB TRUE	[get_ports 	{rx_wb_ack_i}	    ] ;# LA18_P
#set_property IOB TRUE	[get_ports 	{rx_wb_we_o}	    ] ;# LA18_N
#set_property IOB TRUE	[get_ports 	{rx_wb_cyc_o}	    ] ;# LA19_P
#set_property IOB TRUE	[get_ports 	{rx_wb_stb_o}	    ] ;# LA19_N
#set_property IOB TRUE	[get_ports 	{rx_wb_clk_o}	    ] ;# LA20_P
#set_property IOB TRUE	[get_ports 	{tx_wb_clk_o}	    ] ;# LA20_N
#set_property IOB TRUE	[get_ports 	{rx_wb_rty_i}	    ] ;# LA21_P
#set_property IOB TRUE	[get_ports 	{rx_wb_stall_i}	    ] ;# LA21_N
#set_property IOB TRUE	[get_ports 	{tx_wb_adr_o[0]}	] ;# LA22_P
#set_property IOB TRUE	[get_ports 	{tx_wb_adr_o[1]}	] ;# LA22_N
#set_property IOB TRUE	[get_ports 	{tx_wb_adr_o[2]}	] ;# LA23_P
#set_property IOB TRUE	[get_ports 	{tx_wb_adr_o[3]}	] ;# LA23_N
#set_property IOB TRUE	[get_ports 	{tx_wb_adr_o[4]}	] ;# LA24_P
#set_property IOB TRUE	[get_ports 	{tx_wb_adr_o[5]}	] ;# LA24_N
#set_property IOB TRUE	[get_ports 	{tx_wb_adr_o[6]}	] ;# LA25_P
#set_property IOB TRUE	[get_ports 	{tx_wb_adr_o[7]}	] ;# LA25_N
#set_property IOB TRUE	[get_ports 	{tx_wb_dat_b[0]}	] ;# LA26_P
#set_property IOB TRUE	[get_ports 	{tx_wb_dat_b[1]}	] ;# LA26_N
#set_property IOB TRUE	[get_ports 	{tx_wb_dat_b[2]}	] ;# LA27_P
#set_property IOB TRUE	[get_ports 	{tx_wb_dat_b[3]}	] ;# LA27_N
#set_property IOB TRUE	[get_ports 	{tx_wb_dat_b[4]}	] ;# LA28_P
#set_property IOB TRUE	[get_ports 	{tx_wb_dat_b[5]}	] ;# LA28_N
#set_property IOB TRUE	[get_ports 	{tx_wb_dat_b[7]}	] ;# LA29_N
#set_property IOB TRUE	[get_ports 	{rx_wb_err_i}	    ] ;# LA30_P
#set_property IOB TRUE	[get_ports 	{tx_wb_we_o}	    ] ;# LA30_N
#set_property IOB TRUE	[get_ports 	{tx_wb_cyc_o}	    ] ;# LA31_P
#set_property IOB TRUE	[get_ports 	{tx_wb_stb_o}	    ] ;# LA31_N
#set_property IOB TRUE	[get_ports 	{tx_wb_ack_i}	    ] ;# LA32_P
#set_property IOB TRUE	[get_ports 	{tx_wb_err_i}	    ] ;# LA32_N
#set_property IOB TRUE	[get_ports 	{tx_wb_rty_i}	    ] ;# LA33_P
#set_property IOB TRUE	[get_ports 	{tx_wb_stall_i}	    ] ;# LA33_N