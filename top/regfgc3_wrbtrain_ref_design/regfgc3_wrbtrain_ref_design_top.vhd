----------------------------------------------------------------------------------
-- Company: 		CERN
-- Engineer: 		Maciej Lipinski, based on TOP.vhd provided by Matteo Di Cosmo
-- 
-- Create Date:    	11/12/2018 
-- Design Name: 
-- Module Name:    	regfgc_wrbtrain_ref_design - RTL 
-- Project Name:   	RegFGC3 Extension FMC & POF
-- Target Devices: 	Artix-7
-- Tool versions: 	Vivado 2015.4
-- Description: 	This is the top module of the RegFGC3 Extension FMC & POF
--
-- FPGA: XC7A50T-2CSG324C
--
-- Revision: 
-- Revision 0.01 - File Created
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library work;

library UNISIM;
use UNISIM.VComponents.all;
use work.BTrainCarrierInterface_pkg.all;
use work.BTrainFrame_pkg.all;
--=============================================================================
-- Entity declaration for TOP
--=============================================================================
entity regfgc3_wrbtrain_ref_design_top is
	Port(
		FPGA_SYS_CLK_i         : in    STD_LOGIC; -- 40 MHz
		FPGA_AGILENT_D         : inout STD_LOGIC_VECTOR(15 downto 0);
		FPGA_AGILENT_CLK       : inout STD_LOGIC;
		FPGA_EXEC_READ_i       : in    STD_LOGIC_VECTOR(3 downto 0);
		FPGA_OPTION_SW_i       : in    STD_LOGIC_VECTOR(8 downto 1);
		FPGA_FGC3_CMD_i        : in    STD_LOGIC_VECTOR(7 downto 0);
		FPGA_FGC3_STATUS_i     : in    STD_LOGIC_VECTOR(15 downto 0);
		FPGA_QSPI_DIN_i        : in    STD_LOGIC;
		FPGA_QSPI_CLK_i        : in    STD_LOGIC;
		FPGA_QSPI_DOUT_o       : out   STD_LOGIC;
		FPGA_DIG_SUPA_i        : in    STD_LOGIC_VECTOR(4 downto 0);
		FPGA_DIG_SUPB          : inout STD_LOGIC_VECTOR(4 downto 0);
		FPGA_DIR_DIG_SUPB_o    : out   STD_LOGIC_VECTOR(4 downto 0);
		FPGA_POF_TX_1_o        : out   STD_LOGIC;
		FPGA_POF_TX_2_o        : out   STD_LOGIC;
		FPGA_POF_RX_1_i        : in    STD_LOGIC;
		FPGA_POF_RX_2_i        : in    STD_LOGIC;
		BUS_TYPE_i             : in    STD_LOGIC_VECTOR(5 downto 0);
		FPGA_LD1_BLUE_o        : out   STD_LOGIC;
		FPGA_LD2_GREEN_o       : out   STD_LOGIC;
		FPGA_LD1_GREEN_o       : out   STD_LOGIC;
		FPGA_LD2_BLUE_o        : out   STD_LOGIC;
		FPGA_LD2_RED_o         : out   STD_LOGIC;
		FPGA_SCIVS_TX_EN_o     : out   STD_LOGIC;
		FPGA_SCIVS_RX_i        : in    STD_LOGIC;
		FPGA_SCIVS_TX_o        : inout STD_LOGIC;
		FPGA_SCIVS_MODE_i      : in    STD_LOGIC;
		FPGA_SPIVS_TX_o        : inout STD_LOGIC;
		FPGA_SPIVS_CLK_i       : in    STD_LOGIC;
		FPGA_SPIVS_RX_i        : in    STD_LOGIC;
		FPGA_SPIVS_TX_DIR_o    : out   STD_LOGIC;
		-- FMC signals
		FPGA_FMC_PRSNT_M2C_L_i : in    STD_LOGIC;
		FPGA_FMC_TDO_i         : in    STD_LOGIC;
		FPGA_FMC_TCK_o         : out   STD_LOGIC; --ML: to HIGH
		FPGA_FMC_TDI_o         : out   STD_LOGIC; --ML: to HIGH
		FPGA_FMC_TMS_o         : out   STD_LOGIC; --ML: to HIGH
		FPGA_FMC_TRST_L_o      : out   STD_LOGIC; --ML: to HIGH (unconnected
		
		FPGA_FMC_LA_P          : inout   STD_LOGIC_VECTOR(7  downto 0); -- unused banch
		FPGA_FMC_LA_N          : inout   STD_LOGIC_VECTOR(7  downto 0); -- unused banch

        clk_sys_62m5_n_i       : inout      STD_LOGIC;
        clk_sys_62m5_p_i       : inout      STD_LOGIC;
		
		rst_from_carrier_n_o   : out     STD_LOGIC; --rst_from_carrier_i
		rst_to_carrier_n_i     : in      STD_LOGIC; --rst_to_carrier_o

		
		link_up_i              : in      STD_LOGIC;
		time_valid_i           : in      STD_LOGIC;

		irq_aux_i              : in      STD_LOGIC;
		rx_irq_i               : in      STD_LOGIC;
		rx_wb_adr_o            : out     STD_LOGIC_VECTOR(7 downto 0);
		rx_wb_dat_b            : inout   STD_LOGIC_VECTOR(7 downto 0);
		rx_wb_clk_o            : out     STD_LOGIC;
		rx_wb_we_o             : out     STD_LOGIC;
		rx_wb_cyc_o            : out     STD_LOGIC;
		rx_wb_stb_o            : out     STD_LOGIC;
		rx_wb_ack_i            : in      STD_LOGIC;
		rx_wb_err_i            : in      STD_LOGIC;
		rx_wb_rty_i            : in      STD_LOGIC;
		rx_wb_stall_i          : in      STD_LOGIC;
		
		tx_wb_adr_o            : out     STD_LOGIC_VECTOR(7 downto 0);
		tx_wb_dat_b            : inout   STD_LOGIC_VECTOR(7 downto 0);
		tx_wb_clk_o            : out     STD_LOGIC;
		tx_wb_we_o             : out     STD_LOGIC;
		tx_wb_cyc_o            : out     STD_LOGIC;
		tx_wb_stb_o            : out     STD_LOGIC;
		tx_wb_ack_i            : in      STD_LOGIC;
		tx_wb_err_i            : in      STD_LOGIC;
		tx_wb_rty_i            : in      STD_LOGIC;
		tx_wb_stall_i          : in      STD_LOGIC
	);
end regfgc3_wrbtrain_ref_design_top;

--=============================================================================
-- architecture declaration
--=============================================================================
architecture Behavioral of regfgc3_wrbtrain_ref_design_top is

	-- //Clocks and reset
	signal s_reset        : std_logic;
	signal s_rst_n        : std_logic;     
	signal s_Rcounter     : unsigned(31 downto 0) := x"00000000";
	signal clk_sys_62m5   : std_logic;
	
  	signal rx_wb_adr_out  : std_logic_vector(7 downto 0);
    signal rx_wb_dat_in   : std_logic_vector(7 downto 0);
    signal rx_wb_dat_out  : std_logic_vector(7 downto 0);
	signal rx_wb_we_out   : std_logic;
	signal rx_wb_cyc_out  : std_logic;
	signal rx_wb_cyc_out_d: std_logic;
	signal rx_wb_stb_out  : std_logic;
	signal rx_wb_we_out_n : std_logic;

  	signal tx_wb_adr_out  : std_logic_vector(7 downto 0);
    signal tx_wb_dat_in   : std_logic_vector(7 downto 0);
    signal tx_wb_dat_out  : std_logic_vector(7 downto 0);
	signal tx_wb_we_out   : std_logic;
	signal tx_wb_cyc_out  : std_logic;
	signal tx_wb_cyc_out_d: std_logic;
	signal tx_wb_stb_out  : std_logic;	
    signal tx_wb_we_out_n : std_logic;
    
    signal rx_FrameHeader    : t_FrameHeader;
    signal rx_BFramePayload  : t_BFramePayload;
	signal rx_IFramePayload  : t_IFramePayload;
	signal rx_CFramePayload  : t_CFramePayload;
	signal rx_Frame_valid_p1 : std_logic;
	
    signal tx_FrameHeader    : t_FrameHeader;
	signal tx_frame_type     : std_logic_vector(7 downto 0);
    signal tx_BFramePayloads : t_BFramePayload;
	signal tx_IFramePayloads : t_IFramePayload;
	signal tx_CFramePayloads : t_CFramePayload;
    signal tx_ready          : std_logic;
    signal tx_send           : std_logic;
    signal cnt                       : unsigned(15 downto 0);
    signal B_dummy                   : unsigned(31 downto 0);
	
	signal dbg_fms_rx                : std_logic_vector(3 downto 0);
	signal dbg_fms_tx                : std_logic_vector(3 downto 0);
	
	signal PROBE0, PROBE1, PROBE2, PROBE3 : std_logic_vector(31 downto 0);  
	
    COMPONENT my_ila_0
    
    PORT (
        clk : IN STD_LOGIC;
        probe0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0); 
        probe1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0); 
        probe2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        probe3 : IN STD_LOGIC_VECTOR(31 DOWNTO 0)
    );
    END COMPONENT  ;

	  
  signal B_d1       : std_logic_vector(31 downto  0);
  signal B_d2       : std_logic_vector(31 downto  0);
  constant c_zero   : std_logic_vector(31 downto  0) := (others => '0');
  constant c_ones   : std_logic_vector(31 downto  0) := x"00FFFFFF";-- for some reaons the generator counts  with only 24 bits
  signal B_err_cnt  : unsigned(32 downto 0);
  signal B_err      : std_logic;
  
-- =============================================================================
-- architecture begin
-- =============================================================================
begin
	p_ResetCounter : process(FPGA_SYS_CLK_i)
	begin
		if rising_edge(FPGA_SYS_CLK_i) then
			if s_Rcounter /= x"17D78400" then -- 10 sec
				s_Rcounter <= s_Rcounter + 1;
			else
				null;
			end if;
		end if;
	end process p_ResetCounter;

	s_reset <= '0' when (s_Rcounter = x"17D78400") else '1';
	s_rst_n <= '1' when (s_Rcounter = x"17D78400") else '0';
    rst_from_carrier_n_o <= s_rst_n;
	-- DIGSUPB configured as outputs
	FPGA_DIR_DIG_SUPB_o <= (others => '0');
	FPGA_SPIVS_TX_DIR_o <= '0';

	FPGA_POF_TX_1_o <= FPGA_OPTION_SW_i(1);
	FPGA_POF_TX_2_o <= FPGA_OPTION_SW_i(2);
--		OBUFDS_inst : OBUFDS
--			generic map(
--				IOSTANDARD => "LVDS_25")
--			port map(
--				O  => FPGA_FMC_LA_P(0), -- Diff_p output (connect directly to top-level port)
--				OB => FPGA_FMC_LA_N(0), -- Diff_n output (connect directly to top-level port)
--				I  => FPGA_SYS_CLK_i    -- Buffer input 
--			);
	FMC_IO_GEN_UNUSED : for I in 0 to 6 generate
		OBUFDS_inst : OBUFDS
			generic map(
				IOSTANDARD => "LVDS_25")
			port map(
				O  => FPGA_FMC_LA_P(I), -- Diff_p output (connect directly to top-level port)
				OB => FPGA_FMC_LA_N(I), -- Diff_n output (connect directly to top-level port)
				I  => FPGA_SYS_CLK_i    -- Buffer input 
			);
	end generate FMC_IO_GEN_UNUSED;

	FPGA_LD1_GREEN_o <= '0';
	FPGA_LD2_RED_o   <= '0';
	FPGA_LD2_GREEN_o <= '1';
	FPGA_LD2_BLUE_o  <= '1';
    ----------------------------------------------------------------------------
    -- CLKs
    SYS_CLK_INST : IBUFGDS
		port map(
		  O  => clk_sys_62m5,    -- Buffer output
		  I  => clk_sys_62m5_p_i, -- Diff_p buffer input (connect directly to top-level port)
		  IB => clk_sys_62m5_n_i  -- Diff_n buffer input (connect directly to top-level port)
		);
    
    ----------------------------------------------------------------------------
	-- Carrier I/F
	----------------------------------------------------------------------------
	U_BT_CARRIER_IF: BTrainCarrierInterface
    port map(
		clk_i                    => FPGA_SYS_CLK_i,
		rst_n_i                  => s_rst_n,

		-- rx interrupt
		rx_Irq_i                 => rx_irq_i,

		--rx WB Master on
		rx_wb_clk_o              => rx_wb_clk_o,
		rx_wb_adr_o              => rx_wb_adr_out,
		rx_wb_dat_o              => rx_wb_dat_out,
		rx_wb_dat_i              => rx_wb_dat_in,
		rx_wb_cyc_o              => rx_wb_cyc_out,
		rx_wb_stb_o              => rx_wb_stb_out,
		rx_wb_we_o               => rx_wb_we_out,
		rx_wb_ack_i              => rx_wb_ack_i,
		rx_wb_err_i              => rx_wb_err_i,
		rx_wb_rty_i              => rx_wb_rty_i,
		rx_wb_stall_i            => rx_wb_stall_i,

		-- tx WB Master
		tx_wb_clk_o              => tx_wb_clk_o,
		tx_wb_adr_o              => tx_wb_adr_out,
		tx_wb_dat_o              => tx_wb_dat_out,
		tx_wb_dat_i              => tx_wb_dat_in,
		tx_wb_cyc_o              => tx_wb_cyc_out,
		tx_wb_stb_o              => tx_wb_stb_out,
		tx_wb_we_o               => tx_wb_we_out,
		tx_wb_ack_i              => tx_wb_ack_i,
		tx_wb_err_i              => tx_wb_err_i,
		tx_wb_rty_i              => tx_wb_rty_i,
		tx_wb_stall_i            => tx_wb_stall_i,

		-- Interface with BTrain FMC
		rx_PFieldsReadVector_i   => "11111111", -- read all fields
--		rx_PFieldsReadVector_i   => "00000011", -- read first three fields (B, Bdot, Bold)
		rx_Frame_typeID_i        => c_ID_ALL,

		--received from WR network:
		rx_FrameHeader_o         => rx_FrameHeader,
		rx_BFramePayloads_o      => rx_BFramePayload,
		rx_IFramePayloads_o      => rx_IFramePayload,
		rx_CFramePayloads_o      => rx_CFramePayload,
		rx_Frame_valid_p1_o      => rx_Frame_valid_p1,
		rx_Frame_typeID_o        => open,	

		-- transmit to wr network:
		tx_ready_o               => tx_ready,
		tx_PFieldsUpdateVector_i => "11111111",

		tx_TransmitFrame_p1_i    => tx_send,
		tx_FrameHeader_i         => tx_FrameHeader,
		tx_BFramePayloads_i      => tx_BFramePayloads,
		tx_IFramePayloads_i      => tx_IFramePayloads,
		tx_CFramePayloads_i      => tx_CFramePayloads,
		
		dbg_fms_rx_o             => dbg_fms_rx,
		dbg_fms_tx_o             => dbg_fms_tx
  );

  p_dummy_tx_data: process (FPGA_SYS_CLK_i) 
  begin
    if rising_edge(FPGA_SYS_CLK_i) then
      if(s_rst_n = '0') then

        tx_send           <= '0';
        cnt               <=  x"00FA"; -- 4us
        B_dummy           <= (others => '0');
        tx_frame_type     <= (others => '0');
      else

        if(cnt = x"0000") then
     	  if(tx_ready = '1') then
            case B_dummy(1 downto 0) is
			  when "00"   => tx_frame_type <= c_ID_BkFrame;
			  when "01"   => tx_frame_type <= c_ID_ImFrame;
			  when "10"   => tx_frame_type <= c_ID_CdFrame;
			  when "11"   => tx_frame_type <= c_ID_CdFrame;
			  when others => tx_frame_type <= c_ID_BkFrame;
			end case;
			B_dummy       <= B_dummy + 1;
			tx_send       <= '1';
		  end if;
          cnt             <= x"00FA"; -- 4us
        else
            cnt           <= cnt - 1;
            tx_send       <= '0';
        end if;


      end if;
    end if;
  end process;
	
  -- Header
  tx_FrameHeader.frame_type   <= tx_frame_type;
  tx_FrameHeader.sim_eff      <= '0';
  tx_FrameHeader.error        <= '0'; 
  tx_FrameHeader.C0           <= '0';
  tx_FrameHeader.zero_cycle   <= '0';
  tx_FrameHeader.f_low_marker <= '0';
  tx_FrameHeader.d_low_marker <= '0';
  tx_FrameHeader.version_id   <= "00";
  
  --  BFrame
  tx_BFramePayloads.B         <= std_logic_vector(B_dummy) when tx_frame_type = c_ID_BkFrame else 
                                 (others =>'0');
  tx_BFramePayloads.Bdot      <= x"cafecafe";
  tx_BFramePayloads.oldB      <= x"87654321";
  tx_BFramePayloads.measB     <= x"deedbeaf";
  tx_BFramePayloads.simB      <= x"bad0babe";
  tx_BFramePayloads.synB      <= x"00000000";
  
  --  BFrame
  tx_IFramePayloads.I         <= std_logic_vector(B_dummy) when tx_frame_type = c_ID_ImFrame else 
                                 (others =>'0');

  --  CFrame
  tx_CFramePayloads.I         <= std_logic_vector(B_dummy) when tx_frame_type = c_ID_CdFrame else 
                                 (others =>'0');
  tx_CFramePayloads.V         <= x"cafecafe";
  tx_CFramePayloads.MSrcId    <= x"4321";
  tx_CFramePayloads.CSrcId    <= x"5678";
  tx_CFramePayloads.UserData  <= x"bad0babe";
  tx_CFramePayloads.Reserved_1<= x"87654321";
  tx_CFramePayloads.Reserved_2<= x"deadbeef";

  
  ---------------------------------------------------------------------------
  -- check B data

  p_check_rx_data: process (FPGA_SYS_CLK_i) 
  begin
    if rising_edge(FPGA_SYS_CLK_i) then
      if(s_rst_n = '0') then
        B_d1      <= (others => '0');
		B_d2      <= (others => '0');
        B_err     <= '0';
		B_err_cnt <= (others => '0');
      else
	    if(rx_Frame_valid_p1 = '1' and  rx_FrameHeader.frame_type = c_ID_BkFrame) then
          
		  if(B_d1 /= c_zero and B_d1 /= c_ones) then  
		    if( (unsigned(rx_BFramePayload.B) = unsigned(B_d1) + 1) or (unsigned(rx_BFramePayload.B) + 1 = unsigned(B_d1) ) ) then
			  B_err     <= '0';
			else
			  B_err     <= '1';
			  B_err_cnt <= B_err_cnt + 1;
			end if;
		  elsif(B_d1 = c_ones) then
		    if( rx_BFramePayload.B = c_zero ) then
			  B_err     <= '0';
			else
			  B_err     <= '1';
			  B_err_cnt <= B_err_cnt + 1;
			end if;
		  end if;
		  
		  B_d1 <= rx_BFramePayload.B;
		  B_d2 <= B_d1;
		  
		end if;
      end if;
    end if;
  end process;
  
  
  ---------------------------------------------------------------------------
  -- associate inputs/outputs
    rx_wb_adr_o  <= rx_wb_adr_out;
    rx_wb_we_o   <= rx_wb_we_out; 
    rx_wb_cyc_o  <= rx_wb_cyc_out;
    rx_wb_stb_o  <= rx_wb_stb_out;
	
    tx_wb_adr_o  <= tx_wb_adr_out;
    tx_wb_we_o   <= tx_wb_we_out; 
    tx_wb_cyc_o  <= tx_wb_cyc_out;
    tx_wb_stb_o  <= tx_wb_stb_out;
  
    rx_wb_we_out_n <= not rx_wb_we_out;

	INOUT_RX_gen: for I in 0 to 7 generate
      DATA_IOBUF: iobuf
        port map (
         O  => rx_wb_dat_in(i), -- Output (from buffer)
         IO => rx_wb_dat_b(i),  -- Port pin
         I  => rx_wb_dat_out(i),  -- Inuput (to buffer)
         T  => rx_wb_we_out_n);   -- Tristate control
    end generate INOUT_RX_gen;
	
	tx_wb_we_out_n <= not tx_wb_we_out;
	
	INOUT_TX_gen: for I in 0 to 7 generate
      DATA_IOBUF: iobuf
        port map (
         O  => tx_wb_dat_in(i), -- Output (from buffer)
         IO => tx_wb_dat_b(i),  -- Port pin
         I  => tx_wb_dat_out(i),  -- Inuput (to buffer)
         T  => tx_wb_we_out_n);   -- Tristate control
    end generate INOUT_TX_gen;
    ----------------------------------------------------------------------------
    -- snoop
    -- PROBE0( 7 downto  0) <= rx_wb_adr_out;
    -- PROBE0(15 downto  8) <= rx_wb_dat_out;
    -- PROBE0(23 downto 16) <= rx_FrameHeader.frame_type;
    -- PROBE0(          24) <= rx_wb_we_out;
    -- PROBE0(          25) <= rx_wb_cyc_out;
    -- PROBE0(          26) <= rx_wb_stb_out;
    -- PROBE0(          27) <= rx_wb_ack_i;
    -- PROBE0(          28) <= rx_wb_err_i;
    -- PROBE0(          29) <= rx_wb_rty_i;
    -- PROBE0(          30) <= rx_wb_stall_i;
    -- PROBE0(          31) <= rx_irq_i;

	
	-- PROBE1( 7 downto  0) <= rx_wb_dat_in;
	-- PROBE1(31 downto  8) <= rx_CFramePayload.I(23 downto 0);
	-- PROBE2( 7 downto  0) <= tx_frame_type;
	-- PROBE2(15 downto  8) <= tx_wb_dat_in;
	-- PROBE2(31 downto  16) <= std_logic_vector(B_dummy(15 downto 0));

    -- PROBE3( 7 downto  0) <= tx_wb_adr_out;
    -- PROBE3(15 downto  8) <= tx_wb_dat_out;
    -- PROBE3(19 downto 16) <= dbg_fms_rx;
	-- PROBE3(23 downto 20) <= dbg_fms_tx;
    -- PROBE3(          24) <= tx_wb_we_out;
    -- PROBE3(          25) <= tx_wb_cyc_out;
    -- PROBE3(          26) <= tx_wb_stb_out;
    -- PROBE3(          27) <= tx_wb_ack_i;
    -- PROBE3(          28) <= tx_wb_err_i;
    -- PROBE3(          29) <= tx_wb_rty_i;
    -- PROBE3(          30) <= tx_wb_stall_i;
    -- PROBE3(          31) <= tx_send;
	
  ------------------------------------------------------------------------------------
  --snoop
    -- PROBE0( 7 downto  0) <= rx_FrameHeader.frame_type;
    -- PROBE0(           8) <= rx_Frame_valid_p1;
	-- PROBE0(12 downto  9) <= dbg_fms_rx;
    -- PROBE0(15 downto 13) <= (others => '0');
    -- PROBE0(31 downto 16) <= rx_BFramePayload.B(15 downto 0);
	-- PROBE1(15 downto  0) <= rx_IFramePayload.I(15 downto 0);
	-- PROBE1(31 downto 16) <= rx_CFramePayload.I(15 downto 0);
	
	-- PROBE2( 7 downto  0) <= tx_FrameHeader.frame_type;
	-- PROBE2(           8) <= tx_send;
	-- PROBE2(12 downto  9) <= dbg_fms_tx;
    -- PROBE2(15 downto 13) <= (others => '0');
    -- PROBE2(31 downto 16) <= tx_BFramePayloads.B(15 downto 0);
	-- PROBE3(15 downto  0) <= tx_IFramePayloads.I(15 downto 0);
	-- PROBE3(31 downto 16) <= tx_CFramePayloads.I(15 downto 0);
	
	------------------------------------------------------------------------------------
    PROBE0( 7 downto  0) <= rx_FrameHeader.frame_type;
    PROBE0(           8) <= rx_Frame_valid_p1;
	PROBE0(12 downto  9) <= dbg_fms_rx;
	PROBE0(          13) <= B_err;
	PROBE0(          14) <= '0';
	PROBE0(          15) <= '0';
    PROBE0(31 downto 16) <= std_logic_vector(B_err_cnt(15 downto 0));

	PROBE1(31 downto  0) <= rx_BFramePayload.B;
	
	PROBE2(31 downto  0) <= B_d1;
    
	PROBE3( 7 downto  0) <= rx_wb_adr_out;
    PROBE3(15 downto  8) <= rx_wb_dat_out;
	PROBE3(23 downto 16) <= rx_wb_dat_in;
    PROBE3(          24) <= rx_wb_we_out;
    PROBE3(          25) <= rx_wb_cyc_out;
    PROBE3(          26) <= rx_wb_stb_out;
    PROBE3(          27) <= rx_wb_ack_i;
    PROBE3(          28) <= rx_wb_err_i;
    PROBE3(          29) <= rx_wb_rty_i;
    PROBE3(          30) <= rx_wb_stall_i;
    PROBE3(          31) <= rx_irq_i;	

	------------------------------------------------------------------------------------
    -- PROBE0( 7 downto  0) <= rx_FrameHeader.frame_type;
    -- PROBE0(           8) <= rx_Frame_valid_p1;
	-- PROBE0(12 downto  9) <= dbg_fms_rx;
	-- PROBE0(          13) <= B_err;
	-- PROBE0(          14) <= '0';
	-- PROBE0(          15) <= '0';
    -- PROBE0(31 downto 16) <= std_logic_vector(B_err_cnt(15 downto 0));

	-- PROBE1(31 downto  0) <= rx_CFramePayload.I;
	
	-- PROBE2(31 downto  0) <= rx_CFramePayload.V;
    
	-- PROBE3(15 downto  0) <= rx_CFramePayload.CSrcId;
	-- PROBE3(31 downto 16) <= rx_CFramePayload.MSrcId;

	
	
    chipscope: my_ila_0
    port map (
      CLK    => FPGA_SYS_CLK_i, --clk_sys_62m5, --FPGA_SYS_CLK_i,
      PROBE0 => PROBE0,
      PROBE1 => PROBE1,
      PROBE2 => PROBE2,
      PROBE3 => PROBE3 
    );


end Behavioral;
